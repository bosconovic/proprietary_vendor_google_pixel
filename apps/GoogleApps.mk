## Build appropriate year wallpaper package on Pixels
ifneq ($(filter $(PIXEL2016_CODENAMES),$(TARGET_PRODUCT)),)
PRODUCT_PACKAGES += \
    GoogleCameraUltraLegacy \
    Photos
endif
ifneq ($(filter $(PIXEL2017_CODENAMES),$(TARGET_PRODUCT)),)
#PRODUCT_PACKAGES += \
#    NexusWallpapersStubPrebuilt2017
#    GoogleCameraLegacy \
#    Photos
endif
ifneq ($(filter $(PIXEL2018_CODENAMES) $(PIXEL2019_MIDYEAR_CODENAMES),$(TARGET_PRODUCT)),)
PRODUCT_PACKAGES += \
    GoogleCamera \
    Photos
endif
ifneq ($(filter $(PIXEL2019_CODENAMES),$(TARGET_PRODUCT)),)
PRODUCT_PACKAGES += \
    GoogleCamera \
    Photos
endif
ifneq ($(filter $(PIXEL2020_CODENAMES),$(TARGET_PRODUCT)),)
PRODUCT_PACKAGES += \
    GoogleCamera \
    Photos
endif
ifneq ($(filter $(PIXEL2021_CODENAMES),$(TARGET_PRODUCT)),)
PRODUCT_PACKAGES += \
    GoogleCamera \
    Photos
endif
ifneq ($(filter $(PIXEL2021_MIDYEAR_CODENAMES),$(TARGET_PRODUCT)),)
PRODUCT_PACKAGES += \
    GoogleCamera \
    Photos
endif
ifneq ($(filter $(PIXEL2022_CODENAMES),$(TARGET_PRODUCT)),)
PRODUCT_PACKAGES += \
    GoogleCamera \
    Photos
endif
ifneq ($(filter $(PIXEL2022_MIDYEAR_CODENAMES),$(TARGET_PRODUCT)),)
PRODUCT_PACKAGES += \
    GoogleCamera \
    Photos
endif
ifneq ($(filter $(PIXELTABLET_CODENAMES),$(TARGET_PRODUCT)),)
PRODUCT_PACKAGES += \
    GoogleCamera \
    Photos
endif

# AiAi Allowlist
PRODUCT_COPY_FILES += \
    vendor/google_pixel/apps/sysconfig/aiai_allowlist.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/aiai_allowlist.xml \
    vendor/google_pixel/apps/sysconfig/preinstalled-packages-product-pixel-2017-and-newer.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/preinstalled-packages-product-pixel-2017-and-newer.xml

## Core packages
PRODUCT_PACKAGES += \
    FlipendoPrebuilt \
    GoogleExtServicesMobile \
    NexusLauncherRelease \
    NexusLauncherOverlay \
    PlayAutoInstallConfig

## Extra packages
ifneq ($(GMS_MAKEFILE),gms_minimal.mk)
PRODUCT_PACKAGES += \
    RecorderPrebuilt
endif

## Privledged App Permissions
# Not split to account for the fact these apps are put in different locations by different GApps packages
PRODUCT_COPY_FILES += \
    vendor/google_pixel/apps/permissions/privapp-permissions-pixel.xml:system/etc/permissions/privapp-permissions-pixel.xml
PRODUCT_COPY_FILES += \
    vendor/google_pixel/apps/permissions/privapp-permissions-pixel.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-pixel.xml
PRODUCT_COPY_FILES += \
    vendor/google_pixel/apps/permissions/privapp-permissions-pixel.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-pixel.xml
PRODUCT_COPY_FILES += \
    vendor/google_pixel/apps/permissions/privapp-permissions-pixel-s.xml:system/etc/permissions/privapp-permissions-pixel-s.xml
PRODUCT_COPY_FILES += \
    vendor/google_pixel/apps/permissions/privapp-permissions-google-p-pixel.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-google-p-pixel.xml
PRODUCT_COPY_FILES += \
    vendor/google_pixel/apps/permissions/privapp-permissions-google-se-pixel.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-google-se-pixel.xml
PRODUCT_COPY_FILES += \
    vendor/google_pixel/apps/permissions/privapp_allowlist_com.google.android.ext.services.xml:system/etc/permissions/privapp_allowlist_com.google.android.ext.services.xml

PRODUCT_PACKAGE_OVERLAYS += vendor/google_pixel/overlays/overlay-gms
